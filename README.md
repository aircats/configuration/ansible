# ansible
Ansible code for deployment of the static HTTP cats application  
Playbook is in `main.yaml`: uses the `files/aircats.systemd.service` file to allow `systemd` to manage the Golang application.  
Static inventory in `inventory.yaml` which will manage the EC2 instances.

## Structure
```
.
├── README.md
├── ansible.cfg
├── files
│   └── httpcats.systemd.service
├── inventory.yaml
└── main.yaml
```


